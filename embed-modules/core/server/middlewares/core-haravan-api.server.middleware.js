'use strict';

var mongoose = require('mongoose');
var path = require('path');
var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var ShopMD = mongoose.model(config.dbprefix + '_Shop');
var HaravanAPI = require(path.resolve('./modules/haravanapi/server/libs/haravanapi'));
var CoreSession = require(path.resolve('./embed-modules/coreSession/server/businesses/session.server.businesses'));

exports.haravanApi = function(req, res, next) {
  var apiConfig = {
    shop: req.query.shop || '',
    timestamp: req.query.timestamp || '',
    signature: req.query.signature || '',
    code: req.query.code || ''
  };
  req.HaravanAPI = new HaravanAPI(apiConfig);

  var requestShop = req.query.shop ||
    req.headers['haravan-shop-domain'] ||
    req.body.shop ||
    req.session.shop || '';

  if(req.originalUrl.indexOf('/finalize') !== -1 || req.originalUrl.indexOf('/authenticate') !== -1){
    if (!req.HaravanAPI.check_security()) {
      return res.sendStatus(401);
    }else{
      next();
    }
  } else {
    if(req.session.shop && req.session.access_token && requestShop == req.session.shop){
      req.HaravanAPI.config.shop = req.session.shop;
      req.HaravanAPI.config.access_token = req.session.access_token;
      next();
    }else{
      if(requestShop){
        res.redirect(req.HaravanAPI.buildLinkInstallApp(requestShop));
      }else{
        res.sendStatus(401);
      }
    }
  }
};

exports.webhookValidate = function(req, res, next) {
  var shop = req.headers['haravan-shop-domain'] || '';
  var signature = req.headers['x-haravan-hmac-sha256'] || '';
  var topic = req.headers['x-haravan-topic'] || '';

  if(!shop || !signature || !topic) {
    return res.sendStatus(401);
  }

  req.HaravanAPI = new HaravanAPI();

  // validate the request is from haravan
  if (!req.fromHaravan()) {
    return res.status(401).send();
  }

  next();
};

exports.authenticateApi = function(req, res, next) {
  var apiConfig = {
    shop: req.query.shop || '',
    timestamp: req.query.timestamp || '',
    signature: req.query.signature || '',
    code: req.query.code || ''
  };
  req.HaravanAPI = new HaravanAPI(apiConfig);

  var requestShop = req.query.shop ||
    req.headers['haravan-shop-domain'] ||
    req.body.shop ||
    req.session.shop || '';

  if(req.session.shop && req.session.access_token && requestShop == req.session.shop){
    req.HaravanAPI.config.shop = req.session.shop;
    req.HaravanAPI.config.access_token = req.session.access_token;
    next();
  }else{
    if (!req.HaravanAPI.check_security()) {
      return res.sendStatus(401);
    }else{
      ShopMD.load(requestShop, function(err, shopData) {
        if(err) {
          nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
            filename: __filename,
            fn: 'Shop.load',
            shop: requestShop
          });
          return res.sendStatus(401);

        } else if(shopData && shopData._id && shopData.authorize && shopData.authorize.access_token) {
          try{
            CoreSession.setSessionStoreID(req, shopData.store_id);
            req.HaravanAPI.config.shop = shopData._id;
            req.HaravanAPI.config.access_token = shopData.authorize.access_token;
            req.session.shop = shopData._id;
            req.session.access_token = shopData.authorize.access_token;
            req.session.save(function() {
              next();
            });
          }catch(err2){
            if (err2) {
              nlogger.writelog(nlogger.NLOGGER_ERROR, err2, {
                filename: __filename,
                shop: req.HaravanAPI.config.shop
              });
            }
            return res.sendStatus(401);
          }
        } else {
          return res.sendStatus(401);
        }
      });
    }
  }
};
