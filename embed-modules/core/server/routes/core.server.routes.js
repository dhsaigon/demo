'use strict';

module.exports = function (app, appslug) {
  // Root routing
  var coreController = require('../controllers/core.server.controller');

  var hrvApiMiddleware = require('../middlewares/core-haravan-api.server.middleware');

  // Define error pages
  app.route('/' + appslug + '/server-error').get(coreController.renderServerError);

  // Return a 404 for all undefined api, module or lib routes
  app.route('/' + appslug + '/:url(api|modules|lib)/*').get(coreController.renderNotFound);

  //install app
  app.route('/get-data').get(coreController.getData);
  app.route('/').get(coreController.install);
  app.route('/' + appslug + '/authenticate').get(hrvApiMiddleware.haravanApi, coreController.authenticate);
  app.route('/' + appslug + '/finalize').get(hrvApiMiddleware.haravanApi, coreController.finalize);

  //index view
    console.log("appslug " +appslug );
  app.route('/' + appslug + '/*').get(hrvApiMiddleware.haravanApi, coreController.renderIndex);

};
