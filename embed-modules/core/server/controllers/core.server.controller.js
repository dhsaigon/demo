'use strict';

var path = require('path');
var mongoose = require('mongoose');
var url = require('url');
var crypto = require('crypto');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var ShopAPI = require(path.resolve('./modules/haravanapi/server/api/shop/shop.server.api'));
var ScriptTagApi = require(path.resolve('./modules/haravanapi/server/api/script-tag/script-tag.server.api'));
var SettingBus = require(path.resolve('./embed-modules/settings/server/businesses/settings.server.businesses'));
var WebhookBus = require(path.resolve('./embed-modules/webhooks/server/businesses/webhook-init.server.businesses'));
var CoreSession = require(path.resolve('./embed-modules/coreSession/server/businesses/session.server.businesses'));
var ControllerShop = require(path.resolve('./embed-modules/shop/server/controllers/shop.server.controller'));

var ShopMD = mongoose.model(config.dbprefix + '_Shop');


/**
 * Render the embed application page
 */
exports.renderIndex = function (req, res) {
  var shop = req.session.shop;
  var store_id = req.session.store_id;
  var timestamp = req.query.timestamp || '';
  var signature = req.query.signature || '';
  var code = req.query.code || '';
  var embed = config.embed;
  var embedUrl = config.protocol + shop + '/admin/app#/embed/' + config.apikey;
  var domain = url.parse(config.apphost).hostname;
  console.log("embedUrl "+ embedUrl);
  // if(ScriptTagApi){
  //   console.log("SUCCESS ScriptTagApi")
  //     console.log(ScriptTagApi)
  // }else {
  //     console.log("FAIL ScriptTagApi")
  // }
    if (!shop && !timestamp || !signature) {
    return res.sendStatus(401);
  }
  res.render('embed-modules/core/server/views/index', {
    embed: embed,
    embedUrl: embedUrl,
    domain: domain,
    shopname: shop,
    timestamp: timestamp,
    signature: signature,
    code: code,
    store_id: store_id
  });

};

exports.getData = function (req, res) {
    var background_color = "#BC4031";
    var title_text_string = "Special offer ending soon!";
    // var title_text_color = "#FFFFFF";
    var title_text_color = "#FFFF00";
    var button_text_string = "Buy now";
    var button_text_color = "#000000";
    var button_background = "#FFFFFF";
    var button_url = "google.com";
    var theme = "light"; // (only dark or light)
    res.json({
        background_color: background_color,
        title_text_string: title_text_string,
        title_text_color: title_text_color,
        button_text_string: button_text_string,
        button_text_color: button_text_color,
        button_background: button_background,
        button_url: button_url,
        theme: theme
    });

};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).render('embed-modules/core/server/views/500', {
    error: 'Oops! Something went wrong...'
  });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

  res.status(404).format({
    'text/html': function () {
      res.render('embed-modules/core/server/views/404', {
        url: req.originalUrl
      });
    },
    'application/json': function () {
      res.json({
        error: 'Path not found'
      });
    },
    'default': function () {
      res.send('Path not found');
    }
  });
};

/**
 * Install application
 * @param req Incomming Request
 * @param res Response
 */


/**
 * Install application
 * @param req Incomming Request
 * @param res Response
 */
exports.install = function (req, res) {
  if (!req.query.shop) {
      /*var code = "codecode";
      var timestamp = 6434632462;
      var shop = req.query.shop + ".myharavan.com";
      var secret = config.apisecret;
      var signer = crypto.createHmac('sha256', secret);
      var str = '';
      if (code) str = 'code=' + code;

      str += 'shop=' + shop + 'timestamp=' + timestamp;

      var signature = signer.update(str).digest('hex');

      res.redirect(url.format({
          pathname:"/baseapp/embed/authenticate",
          query: {
              shop: shop,
              timestamp: timestamp,
              signature: signature,
              code: code
          }
      }));*/
      res.render('embed-modules/core/server/views/index', {});

  } else {
    res.render('embed-modules/core/server/views/install', {});
  }

};

/**
 * Application Authenticate
 * @param req Incomming Request
 * @param res Response
 */
exports.authenticate = function (req, res) {
  let shop = req.query.shop || '';
 /*ShopMD.load(shop, function(err, shopData) {
    if(shopData && shopData.reintallapp) {

      res.render('embed-modules/core/server/views/redirect', {
        auth_url: req.HaravanAPI.buildAuthURL()
      });
    } else {
      res.redirect(req.HaravanAPI.buildAuthURL());
    }
  });*/
};

/**
 * Finalized
 * @param req Incomming Request
 * @param res Response
 */
exports.finalize = async function (req, res) {
  let shop = req.query.shop || '';
  let tokenData = await getAccessToken(req);
  if(!tokenData) return res.sendStatus(401);

  req.HaravanAPI.config.access_token = tokenData.access_token;
  let shopAPI = new ShopAPI(req.HaravanAPI);
  let scriptTagApi = new ScriptTagApi(req.HaravanAPI);
  var url = req.protocol + '://' + req.get('host')+"/top-ad/js/top-ad-frontend.js";
  var display_scope = "online_store";
    scriptTagApi.list(function(error,list){
        if(error){
            return;
        }
        var exist = false;
        for(var i=0;i<list.length;i++){
            var id = list[i].id;
            var src = list[i].src;
            if(src == url){
                exist = true;
            }
        }
        if(!exist){
            scriptTagApi.create(url,function(error,result){
                if(!error){
                    console.log("result "+ JSON.stringify(result))
                }
            })
        }


    })



  let shopData = await getShopJson(shopAPI);
  if(!shopData) return res.sendStatus(401);

  var authorizeInfo = {
    access_token: tokenData.access_token,
    refresh_token: tokenData.refresh_token,
    expires_in: tokenData.expires_in,
    haravan_settings: shopData
  };
  ShopMD.updateAuthorize(shop, 1, authorizeInfo, function (newShopObj) {
    if(newShopObj && newShopObj._id){
      try{
        CoreSession.setSessionStoreID(req, newShopObj.store_id);
        req.session.shop = shop;
        req.session.store_id = newShopObj.store_id;
        req.session.access_token = tokenData.access_token;
        req.session.save(function (err2) {
          SettingBus.checkAndCreatSetting(req);
          WebhookBus.initWebhooks(req.HaravanAPI);
          return res.redirect('/' + config.appslugembed + '/' + req._parsedUrl.search);
        });
      }catch(err){
        if (err) {
          nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
            filename: __filename,
            shop: req.HaravanAPI.config.shop
          });
        }
        return res.sendStatus(401);
      }
    }else{
      return res.sendStatus(401);
    }
  });
};

async function getAccessToken(req){
  return new Promise((resolve, reject) => {
    req.HaravanAPI.getNewAccessToken(function (err, tokenData) {
      if (err) {
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
          filename: __filename,
          fn: 'getNewAccessToken',
          shop: req.HaravanAPI.config.shop
        });
        resolve();
      } else if (tokenData && tokenData.access_token) {
        resolve(tokenData);
      } else {
        resolve();
      }
    });
  });
};

async function getShopJson(shopAPI){
  return new Promise((resolve, reject) => {
    /*shopAPI.get(function (err, shopData) {
      if (err) {
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
          filename: __filename,
          fn: 'getShopJson',
          shop: shopAPI.config.shop
        });
        resolve();
      }else if (shopData && shopData.id) {
        resolve(shopData);
      }else{
        resolve();
      }
    });*/
  });
};
