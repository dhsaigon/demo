'use strict';

/**
 * Edits by Ryan Hutchison
 * Credit: https://github.com/paulyoder/angular-bootstrap-show-errors */

angular
    .module('core')
    .directive('showErrors', showErrors);

showErrors.$inject = [
  '$timeout',
  '$interpolate'
];

function showErrors($timeout, $interpolate) {
  return {
    restrict: 'A',
    require: '^form',
    compile: compile
  };

  function compile(elem, attrs) {
    if (attrs.showErrors.indexOf('skipFormGroupCheck') === -1) {
      if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
        throw 'show-errors element does not have the \'form-group\' or \'input-group\' class';
      }
    }

    return link;
  }

  function link(scope, elem, attrs, formCtrl) {
    var inputEl;
    var inputName;
    var inputNgEl;
    var options;
    var showSuccess;
    var toggleClasses;
    var initCheck = false;
    var showValidationMessages = false;

    options = scope.$eval(attrs.showErrors) || {};
    showSuccess = options.showSuccess || false;
    inputEl = elem[0].querySelector('.form-control[name]') || elem[0].querySelector('[name]');
    inputNgEl = angular.element(inputEl);
    inputName = $interpolate(inputNgEl.attr('name') || '')(scope);

    if (!inputName) {
      throw 'show-errors element has no child input elements with a \'name\' attribute class';
    }

    var reset = function () {
      return $timeout(function () {
        elem.removeClass('has-error');
        elem.removeClass('has-success');
        showValidationMessages = false;
      }, 0, false);
    };

    scope.$watch(function () {
      return formCtrl[inputName] && formCtrl[inputName].$invalid;
    }, function (invalid) {
      return toggleClasses(invalid);
    });

    scope.$on('show-errors-check-validity', function (event, name) {
      if (angular.isUndefined(name) || formCtrl.$name === name) {
        initCheck = true;
        showValidationMessages = true;

        return toggleClasses(formCtrl[inputName].$invalid);
      }
    });

    scope.$on('show-errors-reset', function (event, name) {
      if (angular.isUndefined(name) || formCtrl.$name === name) {
        return reset();
      }
    });

    toggleClasses = function (invalid) {
      elem.toggleClass('has-error', showValidationMessages && invalid);
      if (showSuccess) {
        return elem.toggleClass('has-success', showValidationMessages && !invalid);
      }
    };
  }
}
