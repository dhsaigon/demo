'use strict';

angular
    .module('core')
    .directive('modal', modal);

function modal() {
  var modalZIndex = 1050;

  return {
    template: '<div class="modal fade">' +
    '<div class="modal-dialog modal-{{ size }}" style="margin-top: {{ top }}px">' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
    '<h4 class="modal-title">{{ title }}</h4>' +
    '</div>' +
    '<div class="modal-body" ng-transclude></div>' +
    '</div>' +
    '</div>' +
    '</div>',
    restrict: 'E',
    transclude: true,
    replace:true,
    scope:true,
    link: link
  };

  function link(scope, elem, attrs) {
    scope.title = attrs.title;
    scope.size = attrs.size;
    scope.top = attrs.top || 30;

    modalZIndex++;

    $(elem).css('z-index', modalZIndex);
    $(elem).attr('data-zindex', modalZIndex);

    scope.$watch(attrs.visible, function(value) {
      if (value == true) {
        $(elem).modal('show');
      } else {
        $(elem).modal('hide');
      }
    });

    $(elem).on('shown.bs.modal', function() {
      var modalIndex = Number($(elem).attr('data-zindex'));

      var modalBackdrop = $('.modal-backdrop:not([data-indexed])');
      modalBackdrop.css('z-index', modalIndex - 1);
      modalBackdrop.attr('data-indexed', true);

      $(elem).animate({ scrollTop: 0 }, 500);

      scope.$apply(function() {
        if (typeof scope.$parent[attrs.visible] == 'boolean') {
          scope.$parent[attrs.visible] = true;
        } else if (typeof scope.$parent[attrs.visible] == 'boolean') {
          scope.$parent.$root[attrs.visible.replace('$root.', '')] = true;
        }
      });
    });

    $(elem).on('hidden.bs.modal', function() {
      $('body').toggleClass('modal-open');

      scope.$apply(function() {
        if (typeof scope.$parent[attrs.visible] == 'boolean') {
          scope.$parent[attrs.visible] = false;
        } else if (typeof scope.$parent.$root[attrs.visible.replace('$root.', '')] == 'boolean') {
          scope.$parent.$root[attrs.visible.replace('$root.', '')] = false;
        }
      });
    });
  }
}
