'use strict';

angular
    .module('core')
    .directive('haravanUpload', haravanUpload);

haravanUpload.$inject = ['FileUploader'];

function haravanUpload (FileUploader) {
  var template = "";
  template += "<section class=\"haravan-upload\">";
  template += "  <div class=\"text-center form-group\" ng-hide=\"uploader.queue.length\">";
  template += "    <span class=\"btn btn-primary btn-file\">";
  template += "      Chọn file <input type=\"file\" nv-file-select uploader=\"uploader\">";
  template += "    <\/span>";
  template += "  <\/div>";
  template += "  <div class=\"text-center form-group\" ng-show=\"uploader.queue.length\">";
  template += "    <button type=\"button\" class=\"btn btn-primary\" ng-click=\"upload();\">{{ uploadText }}<\/button>";
  template += "    <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelUpload();\">{{ cancelText }}<\/button>";
  template += "  <\/div>";
  template += "<\/section>";

  return {
    scope: {
      uploadHandle: '='
    },
    restrict: 'E',
    transclude: true,
    template: template,
    compile: compile
  };

  function compile () {
    return {
      pre: pre
    };

    function pre (scope) {
      if (!scope.uploadHandle || (scope.uploadHandle && typeof scope.uploadHandle !== 'object')) {
        console.warn('Upload handle should\'t be null');
      }

      var fileType = scope.uploadHandle && scope.uploadHandle.fileType
          ? scope.uploadHandle.fileType
          : null;

      var fileSize = scope.uploadHandle && scope.uploadHandle.fileSize
          ? scope.uploadHandle.fileSize
          : 1 * 1024 * 1024;

      // Create file uploader instance
      scope.uploader = new FileUploader({
        url: scope.uploadHandle.url,
        alias: scope.uploadHandle.alias || 'uploadedFile',
        onAfterAddingFile: onAfterAddingFile,
        onSuccessItem: onSuccessItem,
        onErrorItem: onErrorItem
      });

      if (fileType == 'image') {
        // Set file uploader image filter
        scope.uploader.filters.push({
          name: 'imageFilter',
          fn: function imageFilter (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
        });
      } else if (fileType == 'excel') {
        // Set file uploader excel filter
        scope.uploader.filters.push({
          name: 'excelFilter',
          fn: function excelFilter (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|wps-office.xls|wps-office.xlsx|'.indexOf(type) !== -1;
          }
        });
      } else if (fileType == 'word') {
        // Set file uploader word filter
        scope.uploader.filters.push({
          name: 'wordFilter',
          fn: function wordFilter (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|msword|vnd.openxmlformats-officedocument.wordprocessingml.document|wps-office.doc|wps-office.docx|'.indexOf(type) !== -1;
          }
        });
      }

      scope.uploadText = scope.uploadHandle && scope.uploadHandle.uploadText ? scope.uploadHandle.uploadText : 'Upload';
      scope.cancelText = scope.uploadHandle && scope.uploadHandle.cancelText ? scope.uploadHandle.cancelText : 'Huỷ';
      scope.upload = upload;
      scope.cancelUpload = cancelUpload;

      // Called after the user selected a new file
      function onAfterAddingFile (fileItem) {
        if (fileItem && fileItem._file) {
          var validSize = fileItem._file.size <= fileSize;

          if (!validSize) {
            scope.uploader.cancelAll();
            scope.uploader.clearQueue();
          }

          if(scope.uploadHandle.onAdded) {
            scope.uploadHandle.onAdded(fileItem);
          }
        }
      }

      // Called after the user has successfully uploaded a new file
      function onSuccessItem (fileItem, response) {
        if(response && response.error) {
          if(scope.uploadHandle.onError) {
            scope.uploadHandle.onError(fileItem, response);
          }
        } else {
          if(scope.uploadHandle.onSuccess) {
            scope.uploadHandle.onSuccess(fileItem, response);
          }
        }

        // Clear upload buttons
        cancelUpload();
      }

      // Called after the user has failed to uploaded a new file
      function onErrorItem (fileItem, response) {
        // Clear upload buttons
        cancelUpload();

        if(scope.uploadHandle.onError) {
          scope.uploadHandle.onError(fileItem, response);
        }
      }

      function upload() {
        // Start upload
        scope.uploader.uploadAll();

        if(scope.uploadHandle.onUpload) {
          scope.uploadHandle.onUpload();
        }
      }

      // Cancel the upload process
      function cancelUpload () {
        scope.uploader.clearQueue();

        if(scope.uploadHandle.onCancel) {
          scope.uploadHandle.onCancel();
        }
      }

    }
  }
}
