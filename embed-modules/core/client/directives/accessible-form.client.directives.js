'use strict';

angular
    .module('core')
    .directive('accessibleForm', accessibleForm);

function accessibleForm() {
  return {
    restrict: 'A',
    link: link
  };

  function link(scope, elem) {
    // set up event handler on the form element
    elem.on('submit', function () {
      // find the first invalid element
      var firstInvalid = elem[0].querySelector('.ng-invalid');

      // if we find one, set focus
      if (firstInvalid) {
        firstInvalid.focus();
      }
    });
  }
}
