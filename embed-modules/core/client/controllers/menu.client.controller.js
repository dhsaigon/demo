'use strict';

angular.module('core').controller('MenuController', ['$scope', '$location', '$state', 'Menus',
  function ($scope, $location, $state, Menus) {
    // Expose view variables
    $scope.$state = $state;
    $scope.appslug = appslug || 'baseapp';

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');
  }
]);
