'use strict';

var myApp = angular.module('core').controller('MainController', ['$scope', '$location', '$state', 'Menus', '$http',
    function ($scope, $location, $state, Menus, $http) {
        // Expose view variables
        $scope.titletext = '';
        $scope.buttontext = '';
        $scope.buttonurl = '';
        $scope.loopingtime = '';

        $scope.color = '';
        $scope.mcolor = '';
        $scope.styleColor = '';

        $scope.colorbackgroup = '#e63131';
        $scope.colortext = '#ffffff';
        $scope.colorbtnbackgroup = '#ffffff';
        $scope.colortextbtn = '#000000';

        $scope.selectcolorbackgroup = false;
        $scope.selectcolortext = false;
        $scope.selectcolorbtnbackgroup = false;
        $scope.selectcolortextbtn = false;
        //datetime
        $scope.hour_top_1 = '0';
        $scope.hour_bot_1 = '0';
        $scope.hour_top_2 = '0';
        $scope.hour_bot_2 = '0';

        $scope.minutes_top_1 = '0';
        $scope.minutes_bot_1 = '0';
        $scope.minutes_top_2 = '0';
        $scope.minutes_bot_2 = '0';

        $scope.loopingtime = '';
        $scope.domain = '';
        $scope.shop = '';
        $scope.clockTheme = 'dark';
        $scope.enableWidget = false;
        $scope.showWidgetOnHomepageOnly = false;
        $scope.buttonUrl = "";
        $scope.firstChoice = 0;
        $scope.alert = false;
        $scope.alertTitle = 'null';
        $scope.alertContent = 'null';
        $scope.hours = [];
        for(var i=0;i<25;i++){
            $scope.hours.push({
                id: i,
                label: i,
                subItem: { name: "" }
            });
        };

        $scope.minuteOrSecondArray = [];
        for(var i=0;i<61;i++){
            $scope.minuteOrSecondArray.push({
                id: i,
                label: i,
                subItem: { name: "" }
            });
        };
        //end datetime
        //end master data

        $scope.selectColor = function (value) {
            switch (value) {
                case 1:
                    $scope.selectcolorbackgroup = true;
                    $scope.selectcolortext = false;
                    $scope.selectcolorbtnbackgroup = false;
                    $scope.selectcolortextbtn = false;
                    break;
                case 2:
                    $scope.selectcolorbackgroup = false;
                    $scope.selectcolortext = true;
                    $scope.selectcolorbtnbackgroup = false;
                    $scope.selectcolortextbtn = false;
                    break;
                case 3:
                    $scope.selectcolorbackgroup = false;
                    $scope.selectcolortext = false;
                    $scope.selectcolorbtnbackgroup = true;
                    $scope.selectcolortextbtn = false;
                    break;
                case 4:
                    $scope.selectcolorbackgroup = false;
                    $scope.selectcolortext = false;
                    $scope.selectcolorbtnbackgroup = false;
                    $scope.selectcolortextbtn = true;
                    break;
                default:
                    alert('error');
                    break
            }

        };

        function showNameChanged() {
            if ($scope.selectcolorbackgroup) {
                $scope.colorbackgroup = $scope.mcolor;
            }
            if ($scope.selectcolortext) {
                $scope.colortext = $scope.mcolor;
            }
            if ($scope.selectcolorbtnbackgroup) {
                $scope.colorbtnbackgroup = $scope.mcolor;
            }
            if ($scope.selectcolortextbtn) {
                $scope.colortextbtn = $scope.mcolor;
            }
            updateItem();

        }

        function changeTime() {
            // var time = $scope.loopingtime;
            // var timeSetup = new Date($scope.loopingtime).getTime()/1000;
            // var timeCurr = new Date().getTime()/1000;
            // if (timeCurr > timeSetup){
            //     $scope.alert = true;
            //     $scope.alertTitle = 'Thời Gian';
            //     $scope.alertContent = 'Ngày nhập cần phải lớn hơn ngày hôm nay';
            // }else{
            //     $scope.alert = false;
            // }


        }

        function updateItem() {
            if ($scope.domain != '') {
                var data = {
                    shop: $scope.shop,
                    text: $scope.titletext,
                    buttontext: $scope.buttontext,
                    backgroupColor: $scope.colorbackgroup,
                    textcolor: $scope.colortext,
                    buttonbackgroup: $scope.colorbtnbackgroup,
                    buttoncolor: $scope.colortextbtn,
                    looping: $scope.loopingtime,
                    datemillisecond: new Date($scope.loopingtime).getTime()/1000,
                    clock_theme: $scope.clockTheme,
                    enable_widget: $scope.enableWidget,
                    show_widget_on_homepage_only: $scope.showWidgetOnHomepageOnly, //only true or false
                    button_url: $scope.buttonUrl,
                    hide_widget_once_button_click: $scope.hideWidgetOnceButtonClick,
                    hide_widget_from_day: $scope.hideWidgetFromDay,
                    hide_widget_from_hour: $scope.hideWidgetFromHour,
                    hide_widget_from_minute: $scope.hideWidgetFromMinute,
                    hide_widget_from_second: $scope.hideWidgetFromSecond
                }
                console.log("$scope.loopingtime" + $scope.loopingtime);
                console.log("UPDATE");
                //update
                $http.post("https://" + $scope.domain + "/baseapp/embed/api/updateItem", data)
                    .then(function (responseUpdate) {
                        console.log('update ' + responseUpdate.data.message);
                    });
            }
        }

        // function updateTheme(string) {
        //     $scope.clockTheme = string;
        //     updateItem();
        // }
        $scope.updateTheme = function (string) {
            $scope.clockTheme = string;
            updateItem();
        }
        $scope.enableOrDisableWWidget = function () {
            if($scope.enableWidget){
                $scope.enableWidget = false;
            }else {
                $scope.enableWidget = true;
            }
            updateItem();
        }
        $scope.showOrHideOnlyOnHomePage = function () {
            if($scope.showWidgetOnHomepageOnly){
                $scope.showWidgetOnHomepageOnly = false;
            }else {
                $scope.showWidgetOnHomepageOnly = true;
            }
            updateItem();
        }

        $scope.hideOrNotShowAfterClick = function () {
            if($scope.hideWidgetOnceButtonClick){
                $scope.hideWidgetOnceButtonClick = false;
            }else {
                $scope.hideWidgetOnceButtonClick = true;
            }
            updateItem();
        }
        $scope.changeDayHideOrNotShowAfterClick = function () {

            updateItem();
        }
        $scope.checktimer = function(){
            return $scope.alert;
        };

        $scope.getTitle = function(){
            return $scope.alertTitle;
        };

        $scope.getContent = function(){
            return $scope.alertContent;
        };

        $scope.$watch('shop', function (newValue, oldValue) {
            $scope.shop = newValue;
        });

        $scope.$watch('loopingtime', function (newValue, oldValue) {
            var timeCurr = new Date().getTime()/1000;
            var timeSet = new Date(newValue).getTime()/1000;
            console.log("timeCurr "+ timeCurr);
            console.log("timeSet "+ timeSet);
            if((timeSet-timeCurr)<0){
                $scope.alert = true;
                var text = moment().add(2, 'hours').format('MM/DD/YYYY hh:mm:ss a');
                console.log("text "+ text);
                $scope.datemillisecond = new Date(text).getTime()/1000;
                $scope.loopingtime = text;
                return;
            }
            $scope.datemillisecond = new Date(newValue).getTime()/1000;
            $scope.loopingtime = newValue;
            updateItem();
        });

        $scope.$watch('manualPosition', function (checkboxVal) {
            $scope.clockTheme = "light";
            updateItem();
        });
        $scope.$watch('darkThemeRadio', function (checkboxVal) {
            $scope.clockTheme = "dark";
            updateItem();
        });
        $scope.$watch('domain', function (newValue, oldValue) {
            $scope.domain = newValue;

            if ($scope.domain != '') {
                $http.get("https://" + $scope.domain + "/baseapp/embed/api/getItem?shop=" + $scope.shop)
                    .then(function (response) {
                        var data = {
                            store_id: $scope.shop,
                            text: $scope.titletext,
                            buttontext: $scope.buttontext,
                            backgroupColor: $scope.colorbackgroup,
                            textcolor: $scope.colortext,
                            buttonbackgroup: $scope.colorbtnbackgroup,
                            buttoncolor: $scope.colortextbtn,
                            looping: $scope.loopingtime,
                            clock_theme: $scope.clockTheme,
                            enable_widget: $scope.enableWidget,
                            show_widget_on_homepage_only: $scope.showWidgetOnHomepageOnly, //only true or false
                            button_url: $scope.buttonUrl,
                            hide_widget_once_button_click: $scope.hideWidgetOnceButtonClick,
                            hide_widget_from_day: $scope.hideWidgetFromDay,
                            hide_widget_from_hour: $scope.hideWidgetFromHour,
                            hide_widget_from_minute: $scope.hideWidgetFromMinute,
                            hide_widget_from_second: $scope.hideWidgetFromSecond
                        }


                        if (response.data.store_id == '') {
                            //create
                            $http.post("https://" + $scope.domain + "/baseapp/embed/api/createItem", data)
                                .then(function (responseCreate) {

                                });
                        } else {
                            $scope.shop = response.data.store_id;
                            $scope.titletext = response.data.text;
                            $scope.buttontext = response.data.buttontext;
                            $scope.colorbackgroup = response.data.backgroupColor;
                            $scope.colortext = response.data.textcolor;
                            $scope.colorbtnbackgroup = response.data.buttonbackgroup;
                            $scope.colortextbtn = response.data.buttoncolor;
                            $scope.loopingtime = response.data.looping;
                            $scope.clockTheme = response.data.clock_theme;
                            $scope.enableWidget = response.data.enable_widget;
                            $scope.showWidgetOnHomepageOnly = response.data.show_widget_on_homepage_only;
                            $scope.buttonUrl = response.data.button_url;
                            $scope.hideWidgetOnceButtonClick = response.data.hide_widget_once_button_click;
                            $scope.hideWidgetFromDay = response.data.hide_widget_from_day;
                            $scope.hideWidgetFromHour = response.data.hide_widget_from_hour;
                            $scope.hideWidgetFromMinute = response.data.hide_widget_from_minute;
                            $scope.hideWidgetFromSecond = response.data.hide_widget_from_second;

                            /*
                            var dataUpdate = {
                              store_id:  response.data.store_id,
                              text: response.data.text,
                              buttontext: response.data.buttontext,
                              backgroupColor: response.data.backgroupColor,
                              textcolor: response.data.textcolor,
                              buttonbackgroup: response.data.buttonbackgroup,
                              buttoncolor: response.data.buttoncolor,
                              looping: response.data.looping,
                            }
                            //update
                            $http.post("https://"+ $scope.domain +"/baseapp/embed/api/updateItem", dataUpdate)
                            .then(function(responseUpdate){

                            });*/
                        }
                    });
            }
        });
        $scope.$watch('mcolor', showNameChanged);
        $scope.$watch('loopingtime', changeTime);
        $scope.$watch('titletext', updateItem);
        $scope.$watch('buttontext', updateItem);
        $scope.$watch('buttonUrl', updateItem);
        $scope.$watch('hideWidgetFromDay', updateItem);
        $scope.$watch('hideWidgetFromHour', updateItem);

    }


]);

