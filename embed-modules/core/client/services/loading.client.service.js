'use strict';

angular
    .module('core')
    .factory('Loading', Loading);

Loading.$inject = [
  '$timeout',
  '$window',
  '$location'
];

function Loading($timeout, $window, $location) {

  return {
    show: show,
    showMini: showMini,
    showWait: showWait
  };

  function show(flag) {
    var loadingClass = 'loading-page';
    var processingClass = 'loading-processing';
    var completeClass = 'loading-complete';

    if(flag) {
      $('body')
          .addClass(loadingClass);

      $timeout(function() {
        $('body').addClass(processingClass);
      }, 100);
    } else {
      $timeout(function() {
        $('body').addClass(completeClass);

        $timeout(function() {
          $('body')
              .removeClass(loadingClass)
              .removeClass(processingClass)
              .removeClass(completeClass);
        }, 1000);
      }, 500);
    }
  }

  function showMini(elem, flag, loadingTimeout) {
    var currentPosition = $(elem).css('position');

    if(currentPosition == 'static') {
      $(elem).css('position', 'relative');
    }

    if(flag) {
      $(elem).addClass('loading');
      $('<div class="cssload-container"><div class="cssload-speeding-wheel"></div></div>').appendTo(elem);
    } else {
      $timeout(function() {
        $('.cssload-container', elem).remove();
        $(elem).removeClass('loading');
      }, loadingTimeout);
    }
  }

  function showWait(flag) {
    if($window.parent) {
      $window.parent.postMessage({
        functionName: 'hrvShowWait',
        showWaitValue: flag,
        hostName: $location.hostname
      }, '*');
    }
  }

}
