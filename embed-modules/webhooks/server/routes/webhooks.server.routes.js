'use strict';

module.exports = function (app, appslugembed) {

  var path = require('path');
  var config = require(path.resolve('./config/config'));

  // Root routing
  var uninstalledWebhook = require('../controllers/webhooks-uninstalled.server.controller');
  var shopWebhook = require('../controllers/webhooks-shop.server.controller');

  //Middlewares
  var hrvApiMiddleware = require(path.resolve('./embed-modules/core/server/middlewares/core-haravan-api.server.middleware'));
  
  app.all('/' + appslugembed + '/webhooks/*', hrvApiMiddleware.webhookValidate);
  app.route('/' + appslugembed + '/webhooks/app/uninstalled').post(uninstalledWebhook.uninstalled);
  app.route('/' + appslugembed + '/webhooks/shop/update').post(shopWebhook.update);

  //Mark: test webhook 200
  var hook200 = function(req, res){
    res.end('It Works!! Path Hit: ' + req.url);
  };

  //route test
  app.route('/' + appslugembed + '/webhooks/*').post(hook200);

};
