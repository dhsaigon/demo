'use strict';

var path = require('path');
var mongoose = require('mongoose');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Shop = mongoose.model(config.dbprefix + '_Shop');

exports.update = function(req, res) {
  let shop = req.headers['haravan-shop-domain'] || '';
  let store_id = Number(req.headers['x-haravan-shopid'] || 0);
  var shopData = req.body;
  var criteria = {
    _id: shop,
    status: 1
  };
  var updateData = {
    $set: {
      haravan_settings: shopData
    }
  };

  Shop.update(criteria, updateData).exec(function (err) {
    if (err) {
      res.sendStatus(400);

      nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
        filename: __filename,
        fn: 'update',
        criteria: criteria,
        shopData: shopData
      });
    } else {
      res.sendStatus(200);
    }
  });
};
