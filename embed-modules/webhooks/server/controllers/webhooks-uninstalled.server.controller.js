'use strict';

var path = require('path');
var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var mongoose = require('mongoose');
var Shop = mongoose.model(config.dbprefix + '_Shop');
var SessionModel = mongoose.model(config.sessionCollection);

exports.uninstalled = function(req, res) {
  let shop = req.headers['haravan-shop-domain'] || '';
  let store_id = Number(req.headers['x-haravan-shopid'] || 0);
  
  Shop.updateAuthorize(shop, 0, {}, function() {
    res.sendStatus(200);

    SessionModel.remove({ "session.store_id" : store_id }, function (err) {
      if(err){
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
          filename: __filename,
          fn: 'SessionModel.remove',
          store_id: store_id
        });
      }
    });
  });
};
