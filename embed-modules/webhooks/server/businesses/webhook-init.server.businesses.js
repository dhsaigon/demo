'use strict';

var path = require('path');
var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var WebHookAPI = require(path.resolve('./modules/haravanapi/server/api/webhook/webhook.server.api'));

var apphost = process.env.NODE_ENV === 'development' ? config.apphostwebhook : config.apphost;
var pathHook = apphost + '/' + config.appslugembed + '/webhooks/';

var ListWebHooks = [
  //Shop
  {
    topic: 'app/uninstalled',
    address: pathHook + 'app/uninstalled',
    is_active: true
  },
  {
    topic: 'shop/update',
    address: pathHook + 'shop/update',
    is_active: true
  },

  //User Seller
  {
    topic: 'user/create',
    address: pathHook + 'user/create',
    is_active: false
  },
  {
    topic: 'user/update',
    address: pathHook + 'user/update',
    is_active: false
  },
  {
    topic: 'user/delete',
    address: pathHook + 'user/delete',
    is_active: false
  },

  //Customer
  {
    topic: 'customers/create',
    address: pathHook + 'customers/create',
    is_active: false
  },
  {
    topic: 'customers/update',
    address: pathHook + 'customers/update',
    is_active: false
  },
  {
    topic: 'customers/delete',
    address: pathHook + 'customers/delete',
    is_active: false
  },

  //Order
  {
    topic: 'orders/create',
    address: pathHook + 'orders/create',
    is_active: false
  },
  {
    topic: 'orders/updated',
    address: pathHook + 'orders/updated',
    is_active: false
  },
  {
    topic: 'orders/delete',
    address: pathHook + 'orders/delete',
    is_active: false
  },
  {
    topic: 'orders/cancelled',
    address: pathHook + 'orders/cancelled',
    is_active: false
  },
  {
    topic: 'orders/paid',
    address: pathHook + 'orders/paid',
    is_active: false
  },
  {
    topic: 'orders/fulfilled',
    address: pathHook + 'orders/fulfilled',
    is_active: false
  },

  //Refund
  {
    topic: 'refunds/create',
    address: pathHook + 'refunds/create',
    is_active: false
  },

  //Inventory Location Balance
  {
    topic: 'inventorylocationbalances/create',
    address: pathHook + 'inventorylocationbalances/create',
    is_active: false
  },
  {
    topic: 'inventorylocationbalances/update',
    address: pathHook + 'inventorylocationbalances/update',
    is_active: false
  },
  {
    topic: 'inventorylocationbalances/delete',
    address: pathHook + 'inventorylocationbalances/delete',
    is_active: false
  },

  //inventory transaction
  {
    topic: 'inventorytransaction/create',
    address: pathHook + 'inventorytransaction/create',
    is_active: false
  },

  //phiếu điều chuyển
  {
    topic: 'inventorytransfers/create',
    address: pathHook + 'inventorytransfers/create',
    is_active: false
  },
  {
    topic: 'inventorytransfers/update',
    address: pathHook + 'inventorytransfers/update',
    is_active: false
  },
  {
    topic: 'inventorytransfers/delete',
    address: pathHook + 'inventorytransfers/delete',
    is_active: false
  },

  //phiếu điều chỉnh
  {
    topic: 'inventoryadjustments/create',
    address: pathHook + 'inventoryadjustments/create',
    is_active: false
  },
  {
    topic: 'inventoryadjustments/update',
    address: pathHook + 'inventoryadjustments/update',
    is_active: false
  },

  //location
  {
    topic: 'locations/create',
    address: pathHook + 'locations/create',
    is_active: false
  },
  {
    topic: 'locations/update',
    address: pathHook + 'locations/update',
    is_active: false
  },
  {
    topic: 'locations/delete',
    address: pathHook + 'locations/delete',
    is_active: false
  }

];

async function createHook(webhookAPI, hook){
  return new Promise((resolve, reject) => {
    var dataJson = {
      "topic": hook.topic,
      "address": hook.address,
      "format": "json"
    };
    webhookAPI.create(dataJson, function(err, data){
      if(err){
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
          filename: __filename,
          fn: 'createHook',
          data: data
        });
      }
      resolve();
    });
  });
}

async function updateHook(webhookAPI, oldHook, hook){
  return new Promise((resolve, reject) => {
    if(oldHook && oldHook.address == hook.address){
      resolve();
    }else{
      var dataJson = {
        "id": oldHook.id,
        "topic": hook.topic,
        "address": hook.address,
        "format": "json"
      };
      webhookAPI.update(oldHook.id, dataJson, function(err, data){
        if(err){
          nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
            filename: __filename,
            fn: 'updateHook',
            data: data
          });
        }
        resolve();
      });
    }
  });
}

async function getListWebhook(HaravanAPI, webhookAPI, hook){
  return new Promise((resolve, reject) => {
    webhookAPI.getList({topic:hook.topic}, function(err, lstHook){
      if(err){
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
          filename: __filename,
          fn: 'initWebhooks',
          shop: HaravanAPI.config.shop
        });
      }
      resolve(lstHook);
    });
  });
}

module.exports.initWebhooks = async function (HaravanAPI, callback) {
  try {
    var webhookAPI = new WebHookAPI(HaravanAPI);
    for(var i=0; i < ListWebHooks.length; i++){
      var hook = ListWebHooks[i];
      if(hook && hook.is_active){
        var lstHook = await getListWebhook(HaravanAPI, webhookAPI, hook);
        if(lstHook && lstHook.length){
          await updateHook(webhookAPI, lstHook[0], hook);
        }else{
          await createHook(webhookAPI, hook);
        }
      }
    }
    if(callback) callback();
  } catch (e) {
    nlogger.writelog(nlogger.NLOGGER_ERROR, e, {
      filename: __filename,
      fn: 'initWebhooks'
    });
    if(callback) callback();
  }
};










