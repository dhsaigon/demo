'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('settings', ['core']);
ApplicationConfiguration.registerModule('settings.routes', ['core']);
