/**
 * Created by User on 4/25/2017.
 */
'use strict';

angular
  .module('core')
  .factory('SettingsApi', SettingsApi);

SettingsApi.$inject = [
  '$resource'
];

function SettingsApi ($resource) {
  return $resource(
    appslug + '/api/settings',
    {
      shop: shopname || '',
      timestamp: timestamp || '',
      signature: signature || '',
      code: code || ''
    },
    {
      query: {
        method: 'GET',
        isArray: false
      },
      update: {
        method: 'PUT',
        isArray: false
      }
    }
  );
}
