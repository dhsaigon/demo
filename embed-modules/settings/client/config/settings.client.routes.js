'use strict';

// Setting up route
angular.module('settings.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('settings', {
        templateUrl: appslug + '/' + 'embed-modules/settings/client/view/settings.client.view.html',
        controller: 'SettingsController',
        params: {},
        data: {
          pageTitle: ''
        }
      });
  }
]);
