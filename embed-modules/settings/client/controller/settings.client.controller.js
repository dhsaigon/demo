'use strict';

angular.module('settings').controller('SettingsController', SettingsController);

SettingsController.$inject = [
  '$scope',
  '$state',
  '$rootScope',
  '$timeout',
  'Loading',
  'ToastMessage',
  'SettingsApi'
];

function SettingsController($scope, $state, $rootScope, $timeout, Loading, ToastMessage, SettingsApi) {
  $scope.setting = {};

  $scope.update = function (isValid) {
    if (!isValid) {
      $scope.$broadcast('show-errors-check-validity', 'settingForm');

      return false;
    }

    var settingObj = new SettingsApi($scope.setting);

    Loading.show(true);
    settingObj.$update(function (data) {
      Loading.show(false);
      if(data && data.store_id){
        $scope.setting = data;
        ToastMessage.info('Update success!!!');
      }else{
        if(data && data.err && data.message){
          ToastMessage.error(data.message);
        }else{
          ToastMessage.error('Không thể cập nhật dữ liệu lúc này!');
        }
      }
    });
  };

  $scope.loadSettings = function() {
    Loading.show(true);
    $scope.setting = {};

    SettingsApi.get({}, function(data) {
      Loading.show(false);
      if(data && data.store_id){
        $scope.setting = data;
      }else{
        if(data && data.err && data.message){
          ToastMessage.error(data.message);
        }else{
          ToastMessage.error('Không thể tải dữ liệu lúc này!');
        }
      }
    }, function () {
      Loading.show(false);
      ToastMessage.error('Không thể tải dữ liệu lúc này!');
    });
  };

  $scope.loadSettings();
}
