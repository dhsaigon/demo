'use strict';

/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var CoreSession = require(path.resolve('./embed-modules/coreSession/server/businesses/session.server.businesses'));

var SettingMD = mongoose.model(config.dbprefix + '_Settings');

exports.create = function (req, res) {
  try{
    let dataSetting = CoreSession.criteriaAddSessionStoreID(req, req.body);
    SettingMD.createSetting(CoreSession.getSessionStoreID(req), dataSetting, function (err, newData) {
      if (err) {
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {filename: __filename, fn: 'createSetting', data: dataSetting});
        res.json({err: true, message: 'ERROR CREATE!'});
      } else {
        res.json(newData);
      }
    });
  }catch(err){
    nlogger.writelog(nlogger.NLOGGER_ERROR, err);
    return res.json({err: true, message: 'Đã có lỗi sảy ra!'});
  }
};

exports.update = function (req, res) {
  try{
    let dataSetting = CoreSession.criteriaAddSessionStoreID(req, req.body);
    let title = req.body.title || '';
    if(!title)  return res.json({err: true, message: 'title không được để trống !'});
    
    SettingMD.updateSetting(CoreSession.getSessionStoreID(req), dataSetting, function (err, data) {
      if (err) {
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {filename: __filename, fn: 'updateSetting', data: dataSetting});
        res.json({err: true, message: 'ERROR UPDATE!'});
      } else {
        res.json(data);
      }
    });
  }catch(err){
    nlogger.writelog(nlogger.NLOGGER_ERROR, err);
    return res.json({err: true, message: 'Đã có lỗi sảy ra!'});
  }
};

exports.detail = function (req, res) {
  try{
    SettingMD.getSetting(CoreSession.getSessionStoreID(req), function (err, data) {
      if (err) {
        nlogger.writelog(nlogger.NLOGGER_ERROR, err, {filename: __filename, fn: 'getSetting', store_id: req.session.store_id});
        res.json({err: true, message: 'ERROR FIND!'});
      } else {
        res.json(data);
      }
    });
  }catch(err){
    nlogger.writelog(nlogger.NLOGGER_ERROR, err);
    return res.json({err: true, message: 'Đã có lỗi sảy ra!'});
  }
};

