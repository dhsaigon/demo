'use strict';

/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var CoreSession = require(path.resolve('./embed-modules/coreSession/server/businesses/session.server.businesses'));
var SettingMD = mongoose.model(config.dbprefix + '_Settings');

exports.checkAndCreatSetting = function (req) {
  try{
    SettingMD.findOne({store_id: CoreSession.getSessionStoreID(req)}).exec(function (err, found) {
      if (!found) {
        let dataSetting = CoreSession.criteriaAddSessionStoreID(req, {});
        SettingMD.createSetting(CoreSession.getSessionStoreID(req), dataSetting, function (err, newData) {
          if (err) nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
            filename: __filename,
            fn: 'createSetting',
            data: dataSetting
          });
        });
      }
    });
  }catch(err){
    nlogger.writelog(nlogger.NLOGGER_ERROR, err);
  }
};