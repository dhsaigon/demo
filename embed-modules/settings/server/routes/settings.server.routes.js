'use strict';

/**
 * Module dependencies
 */
var path = require('path');
var config = require(path.resolve('./config/config'));
var hrvMiddleware = require(path.resolve('./embed-modules/core/server/middlewares/core-haravan-api.server.middleware'));
var settings = require('../controllers/settings.server.controller');

module.exports = function (app, appslug) {
  // Single routes
  app.route('/' + appslug + '/api/settings')
      .get(hrvMiddleware.authenticateApi, settings.detail)
      .put(hrvMiddleware.authenticateApi, settings.update);

};
