'use strict';

/**
 * Module dependencies.
 */
var path = require('path');
var _ = require('lodash');
var mongoose = require('mongoose');
var config = require(path.resolve('./config/config'));
var Schema = mongoose.Schema;

/**
 *  SettingSchema Schema
 */
var SettingSchema = new Schema({
  store_id: { type: Number, required: true, index: { unique: true } },
  created_at : { type : Date, default : Date.now },
  updated_at: { type: Date, default: Date.now },
  title: { type: String, default : ''  }
});
//const INDEX_SETTING_1 = { 'store_id' : 1};
//SettingSchema.index(INDEX_SETTING_1);

/**
 **CRUD
 */
SettingSchema.statics.createSetting =  function (store_id, Data, cb) {
  var _this = this;
  if(store_id && store_id > 0){
    Data.store_id = store_id;
    _this.create(Data, cb);
  }else{
    cb(new Error("store_id is null!"));
  }
};

SettingSchema.statics.updateSetting =  function (store_id, Data, cb) {
  var _this  = this;
  if(store_id && store_id > 0){
    var options = {
      new: true,
      upsert: true
    };
    Data.store_id = store_id;
    Data.updated_at = _.now();
    _this.findOneAndUpdate({store_id: store_id}, {'$set': Data}, options, function (err, newObj) {
      cb(err, newObj);
    });
  }else{
    cb(new Error("store_id is null!"));
  }
};

SettingSchema.statics.getSetting =  function (store_id, cb) {
  var _this = this;
  if(store_id && store_id > 0){
    _this.findOne({store_id : store_id}, function (err, data) {
      cb(err, data);
    });
  }else{
    cb(new Error("store_id is null!"));
  }
};

mongoose.model(config.dbprefix + '_Settings', SettingSchema);
