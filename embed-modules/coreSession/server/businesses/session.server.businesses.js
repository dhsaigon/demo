'use strict';

/**
 * Module dependencies
 */

exports.getSessionStoreID = function (req) {
  let store_id = 0;
  if(req && req.session && req.session.store_id){
    store_id = Number(req.session.store_id);
  }else{
    throw new Error('store_id is null!');
  }
  return store_id;
};

exports.setSessionStoreID = function (req, store_id) {
  if(req && store_id){
    req.session.store_id = Number(store_id);
  }else{
    throw new Error('store_id is null!');
  }
  return store_id;
};

exports.criteriaAddSessionStoreID = function (req, criteria) {
  let store_id = 0;
  if(req && req.session && req.session.store_id){
    store_id = Number(req.session.store_id);
  }else{
    throw new Error('store_id is null!');
  } 

  if(criteria)  criteria.store_id = store_id;
  
  return criteria;
};



