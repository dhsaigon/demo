'use strict';

module.exports = function(app, appslug) {

  var path = require('path');
  var hrvMiddleware = require(path.resolve('./embed-modules/core/server/middlewares/core-haravan-api.server.middleware'));
  var shopController = require('../controllers/shop.server.controller');

  // Define application route
  app.route('/' + appslug + '/api/shop')
      .get(hrvMiddleware.authenticateApi, shopController.shopByName, shopController.read);

};
