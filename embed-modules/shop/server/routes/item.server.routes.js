'use strict';

module.exports = function(app, appslug) {

  var path = require('path');
  var hrvMiddleware = require(path.resolve('./embed-modules/core/server/middlewares/core-haravan-api.server.middleware'));
  var itemController = require('../controllers/item.server.controller');

  // Define application route
  //app.route('/' + appslug + '/api/item')
  //    .get(hrvMiddleware.authenticateApi, itemController.getCollect);
  //create data
  app.route('/' + appslug + '/api/getItem')
      .get(itemController.getItem, itemController.read);

  app.route('/' + appslug + '/api/createItem')
      .post(hrvMiddleware.authenticateApi, itemController.createItem);

  app.route('/' + appslug + '/api/updateItem')
      .post(hrvMiddleware.authenticateApi, itemController.updateItem);

  app.route('/' + appslug + '/api/getStoreId')
      .get(itemController.getStoreId);

};
