'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var Schema = mongoose.Schema;

var item = require('../models/item.server.model');

var itemModel = mongoose.model(config.dbprefix + '_Item');

exports.createItem = function (req, res) {
  itemModel.createitem(req.body, function (err) {
    if (err) {
      res.json(err);
    }
  })
  res.json({err: false, message: 'successfully'});
};

exports.updateItem = function (req, res) {
  itemModel.updateitem(req.body, function (err) {
    if (err) {
      res.json(err);
    }
  })
  res.json({err: false, message: 'successfully'});
};

exports.read = function (req, res) {
  if(req.model !== ''){
    res.json(req.model)
  } else{
    res.json({store_id: '', err: true, message: 'cannot collect'});  
  }
  
};

exports.getItem = function (req, res, next) {
  itemModel.getitem(req.query, function (err, foundItem) {
    if (err) {
      return next(err);
    } else if (!foundItem) {
      req.model = '';
      return next();
    }
    req.model = foundItem;
    next();
  });
};

exports.getStoreId = function (req, res) {
  return res.json({store_id: req.session.shop});
};


