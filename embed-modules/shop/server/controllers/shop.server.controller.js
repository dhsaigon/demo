'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Shop = mongoose.model(config.dbprefix + '_Shop');

exports.read = function (req, res) {
  res.json(req.model);
};

/**
 * Shop middleware
 */
exports.shopByName = function (req, res, next) {
  var shopName = req.session.shop;

  if (shopName.indexOf('.myharavan.com') == -1 && shopName.indexOf('.sku.vn') == -1) {
    return res.status(400);
  }

  Shop.load(shopName, function (err, foundShop) {
    if (err) {
      nlogger.writelog(nlogger.NLOGGER_ERROR, err, { filename: __filename, fn: 'shopByName', data: JSON.stringify({ shopName: shopName }) });

      return next(err);
    } else if (!foundShop) {
      return next(new Error('Failed to load shop ' + shopName));
    }

    req.model = foundShop;
    next();
  });
};
