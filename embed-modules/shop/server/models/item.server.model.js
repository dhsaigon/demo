'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;

/**
 * Shop Schema
 */
var ItemSchema = new Schema({
  store_id: {type: String, unique: true},
  text: String,
  buttontext: String,
  backgroupColor: String,
  textcolor: String,
  buttonbackgroup: String,
  buttoncolor: String,
  datetime: Date,
  looping: String,
  datemillisecond: Number,
  clock_theme: String, //only dark or light
  enable_widget: Boolean, //only true or false
  show_widget_on_homepage_only: Boolean, //only true or false
  button_url: String, //only true or false
    hide_widget_once_button_click: Boolean, //only true or false
    hide_widget_from_day: String,
    hide_widget_from_hour: String,
    hide_widget_from_minute: String,
    hide_widget_from_second: String
});


ItemSchema.statics.createitem = function(item, callback) {
  
  var ItemModel = mongoose.model(config.dbprefix + '_Item', ItemSchema);
  var user = new ItemModel(item);
  user.save(function (err) {
      if (err) return err;
   });
  
};

ItemSchema.statics.updateitem = function(item, callback) {
  	
  	var ItemModel = mongoose.model(config.dbprefix + '_Item', ItemSchema);

	ItemModel.findOne({ store_id: item.shop }, function (err, result){
		if (err) return err;
		if(result !== null){
			result.set(item);
	  	result.save();
		}
	});
};

ItemSchema.statics.getitem = function(item, callback) {
	var ItemModel = mongoose.model(config.dbprefix + '_Item', ItemSchema);
  var criteria = {
    firstName: "Bob"
  };
  ItemModel.findOne(criteria).exec(callback);
};


mongoose.model(config.dbprefix + '_Item', ItemSchema);

