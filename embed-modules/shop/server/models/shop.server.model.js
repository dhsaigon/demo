'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;

/**
 * Shop Schema
 */
var ShopSchema = new Schema({
  _id: { type: String, required: true, index: { unique: true } },
  authorize: {
    access_token: { type: String, default: '' },
    refresh_token: { type : String, default : '' },
    expires_in: { type : Number, default : 0 }
  },
  status: { type: Number, default: 0 },
  haravan_settings: { type: Schema.Types.Mixed },
  store_id: { type: Number, required: true, index: { unique: true } },
  reintallapp: { type: Number, default: 0 }
});

ShopSchema.statics.load = function(shop, callback) {
  var _this = this;
  var criteria = {
    _id: shop,
    status: 1
  };
  _this.findOne(criteria).exec(callback);
};

ShopSchema.statics.loadByStoreId = function(store_id, callback) {
  var _this = this;
  var criteria = {
    store_id: store_id,
    status: 1
  };
  _this.findOne(criteria).exec(callback);
};

ShopSchema.statics.filter = function(page, limit, criteria, sort, callback) {
  var _this = this;
  var page = page || 1;
  var limit = Number(limit) || 20;
  var criteria = criteria || {};
  var sort = sort || { _id: -1 };
  var skip = (page * limit) - limit;

  _this.count(criteria).exec(function(err, count) {
    if(err) {
      return callback(err);
    } else if(count == 0) {
      return callback(null, {
        data: {},
        total: 0
      });
    }

    _this.find(criteria)
        .skip(skip)
        .limit(limit)
        .sort(sort)
        .exec(function(err, data) {
          if(err) {
            return callback(err);
          } else if(data) {
            return callback(null, {
              data: data,
              total: count
            });
          }else{
            return callback(null, {
              data: {},
              total: 0
            });
          }
        });
  });
};

ShopSchema.statics.updateAuthorize = function(shop, status, dataInfo, callback) {
  var _this = this;
  var dataInfo = dataInfo || {};

  var updateData = {
    _id: shop,
    authorize: {
      access_token: dataInfo.access_token || '',
      refresh_token: dataInfo.refresh_token || '',
      expires_in: dataInfo.expires_in || 0
    },
    status: status,
    reintallapp: 0
  };

  if (dataInfo.haravan_settings) {
    updateData.haravan_settings = dataInfo.haravan_settings;
    updateData.store_id = dataInfo.haravan_settings.id;
  }

  var options = {
    new: true,
    upsert: true
  };

  _this.findOneAndUpdate({
    _id: shop
  }, { $set: updateData }, options, function(err, newAuthorize) {
    if(err) {
      nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
        filename: __filename,
        fn: 'updateAuthorize',
        shop: shop
      });
      return callback();
    } else {
      return callback(newAuthorize);
    }
  });
};


mongoose.model(config.dbprefix + '_Shop', ShopSchema);
