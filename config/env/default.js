'use strict';

module.exports = {
  secure: {
    ssl: false
  },
  port: process.env.PORT || 3000,
  host: process.env.HOST || 'localhost',
  app: {
    title: 'Research Havaran',
    description: 'Research Api Havaran',
    version: '1.0.0'
  },
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://app-haravan:Huynh865221@ds115442.mlab.com:15442/app-haravan'|| 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/baseapp',
    options: {
      useMongoClient: true,
      //server: { poolSize: 5, socketOptions: { keepAlive: 1 } },
      user: 'app-haravan',
      pass: 'Huynh865221',
      socketTimeoutMS: 0,
      keepAlive: true,
      reconnectTries: 30
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || false
  },
  log: {
    // logging with Morgan - https://github.com/expressjs/morgan
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: process.env.LOG_FORMAT || 'dev'
  },
  mailer: {
    from: process.env.MAILER_FROM || 'noreply@techstormsoftware.com',
    options: {
      service: process.env.MAILER_SERVICE_PROVIDER || 'http://42.117.4.246:5000/mailer',
      auth: {
        user: process.env.MAILER_EMAIL_ID || '',
        pass: process.env.MAILER_PASSWORD || ''
      }
    }
  },
  templateEngine: 'swig',
  // Session Cookie settings
  sessionCookie: {
    // session expiration is set by default to 24 hours
    maxAge: 24 * (60 * 60 * 1000),
    // httpOnly flag makes sure the cookie is only accessed
    // through the HTTP protocol and not JS/browser
    httpOnly: true,
    // secure cookie should be turned to true to provide additional
    // layer of security so that the cookie is set only when working
    // in HTTPS mode.
    secure: false
  },
  // sessionSecret should be changed for security measures and concerns
  sessionSecret: process.env.SESSION_SECRET || 'BASE_APP',
  // sessionKey is set to the generic sessionId key used by PHP applications
  // for obsecurity reasons
  sessionKey: 'baseapp',
  sessionCollection: 'baseapp_sessions',
  // Lusca config
  csrf: {
    csrf: false,
    csp: false,
    xframe: 'ALLOWALL',
    p3p: 'ABCDEF',
    hsts: {
      maxAge: 31536000, // Forces HTTPS for one year
      includeSubDomains: true,
      preload: true
    },
    xssProtection: false,
    nosniff: false
  },
  etag: true,
  logo: {
    embed: 'embed-modules/core/client/img/brand/logo.png'
  },
  favicon: {
    embed: 'embed-modules/core/client/img/brand/favicon.ico'
  },
  downloads: {
    dest: './media/downloads'
  },
  appslug: 'baseapp',
  appslugembed: 'baseapp/embed',
  appslugfrontend: 'baseapp/frontend',
  appslugadmin: 'baseapp/admin',
  apphost: 'https://app-haravan.herokuapp.com',
  apikey: 'e27691bf8654f9749a3c35b860f33b63',
  apisecret: '0d0e2c75d14c97e71029b8285abb6a56',
  scope: 'read_products,write_products,read_customers,write_customers,read_orders,write_orders,read_script_tags,write_script_tags,read_fulfillments,write_fulfillments,read_shipping,write_shipping,read_users,write_inventory,read_inventory',
  dbprefix: 'baseapp',
  embed: true,
  verbose: true,
  protocol: 'https://',
  redirect_uri: 'https://app-haravan.herokuapp.com/baseapp/embed/finalize',
  apphostwebhook: 'https://app-haravan.herokuapp.com/'
};
