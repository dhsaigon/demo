'use strict';

/**
 * Module dependencies.
 */
var fs = require('fs');
var path = require('path');

var _ = require('lodash');
var chalk = require('chalk');
var glob = require('glob');


/**
 * Get files by glob patterns
 */
var getGlobbedPaths = function (globPatterns, excludes) {
  // URL paths regex
  var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

  // The output array
  var output = [];

  // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
  if (_.isArray(globPatterns)) {
    globPatterns.forEach(function (globPattern) {
      output = _.union(output, getGlobbedPaths(globPattern, excludes));
    });
  } else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns);
    } else {
      var files = glob.sync(globPatterns);
      if (excludes) {
        files = files.map(function (file) {
          if (_.isArray(excludes)) {
            for (var i in excludes) {
              if (excludes.hasOwnProperty(i)) {
                file = file.replace(excludes[i], '');
              }
            }
          } else {
            file = file.replace(excludes, '');
          }
          return file;
        });
      }
      output = _.union(output, files);
    }
  }

  return output;
};

/**
 * Validate NODE_ENV existence
 */
var validateEnvironmentVariable = function () {
  var environmentFiles = glob.sync("./config/env/app-haravan.js");

  if (!environmentFiles.length) {
    if (process.env.NODE_ENV) {
      console.error(chalk.red('+ Error: No configuration file found for "' + process.env.NODE_ENV + '" environment using development instead'));
    } else {
      console.error(chalk.red('+ Error: NODE_ENV is not defined! Using default development environment'));
    }

    process.env.NODE_ENV = 'app-haravan';
  }

  // Reset console color
};

/**
 * Validate Session Secret parameter is not set to default in production
 */
var validateSessionSecret = function (config, testing) {

  if (process.env.NODE_ENV !== 'app-haravan') {
    return true;
  }

  if (config.sessionSecret === 'MEAN') {
    if (!testing) {
      console.log(chalk.red('+ WARNING: It is strongly recommended that you change sessionSecret config while running in production!'));
      console.log(chalk.red('  Please add `sessionSecret: process.env.SESSION_SECRET || \'super amazing secret\'` to '));
      console.log(chalk.red('  `config/env/production.js` or `config/env/local.js`'));
      console.log();
    }
    return false;
  } else {
    return true;
  }
};

/**
 * Initialize global configuration files
 */
var initGlobalConfigFolders = function (config, assets) {
  // Appending files
  config.folders = {
    server: {},
    client: {},
    clientEmbed: {},
    clientFrontEnd: {}
  };

  // Setting globbed client paths
  config.folders.client = getGlobbedPaths(
      path.join(process.cwd(), 'modules/*/client/'),
      process.cwd().replace(new RegExp(/\\/g), '/')
  );

  config.folders.clientEmbed = getGlobbedPaths(
      path.join(process.cwd(), 'embed-modules/*/client/'),
      process.cwd().replace(new RegExp(/\\/g), '/')
  );

  config.folders.clientFrontEnd = getGlobbedPaths(
      path.join(process.cwd(), 'frontend-modules/*/client/'),
      process.cwd().replace(new RegExp(/\\/g), '/')
  );
};

/**
 * Initialize global configuration files
 */
var initGlobalConfigFiles = function (config, assets) {
  // Appending files
  config.files = {
    server: {},
    client: {}
  };

  // Setting Globbed model files
  config.files.server.models = getGlobbedPaths(assets.server.models);

  // Setting Globbed route files
  config.files.server.routes = getGlobbedPaths(assets.server.routes);

  // Setting Globbed config files
  config.files.server.configs = getGlobbedPaths(assets.server.config);

  // Setting Globbed socket files
  config.files.server.sockets = getGlobbedPaths(assets.server.sockets);

  // Setting Globbed js files
  config.files.client.js = getGlobbedPaths(assets.client.lib.js, 'public/')
      .concat(getGlobbedPaths(assets.client.js, ['public/']));
  config.files.client.jsEmbed = getGlobbedPaths(assets.client.lib.jsEmbed, 'public/')
      .concat(getGlobbedPaths(assets.client.jsEmbed, ['public/']));
  config.files.client.jsFrontEnd = getGlobbedPaths(assets.client.lib.jsFrontEnd, 'public/')
      .concat(getGlobbedPaths(assets.client.jsFrontEnd, ['public/']));

  // Setting Globbed css files
  config.files.client.css = getGlobbedPaths(assets.client.lib.css, 'public/')
      .concat(getGlobbedPaths(assets.client.css, ['public/']));
  config.files.client.cssEmbed = getGlobbedPaths(assets.client.lib.cssEmbed, 'public/')
      .concat(getGlobbedPaths(assets.client.cssEmbed, ['public/']));
  config.files.client.cssFrontEnd = getGlobbedPaths(assets.client.lib.cssFrontEnd, 'public/')
      .concat(getGlobbedPaths(assets.client.cssFrontEnd, ['public/']));
};

/**
 * Initialize global configuration
 */
var initGlobalConfig = function () {
  // Validate NODE_ENV existence
  validateEnvironmentVariable();

  // Get the default assets
  var defaultAssets = require(path.join(process.cwd(), 'config/assets/default'));

  // Get the current assets
  var environmentAssets = require(path.join(process.cwd(), 'config/assets/', 'app-haravan')) || {};

  // Merge assets
  var assets = _.merge(defaultAssets, environmentAssets);

  // Get the default config
  var defaultConfig = require(path.join(process.cwd(), 'config/env/default'));

  // Get the current config
  var environmentConfig = require(path.join(process.cwd(), 'config/env/', 'app-haravan')) || {};

  // Merge config files
  var config = _.merge(defaultConfig, environmentConfig);

  // read package.json for MEAN.JS project information
  config.meanjs = require(path.resolve('./package.json'));

  // Extend the config object with the local-NODE_ENV.js custom/local environment. This will override any settings present in the local configuration.
  config = _.merge(config, (fs.existsSync(path.join(process.cwd(), 'config/env/local-' + 'app-haravan' + '.js')) && require(path.join(process.cwd(), 'config/env/local-' + 'app-haravan' + '.js'))) || {});

  // Initialize global globbed files
  initGlobalConfigFiles(config, assets);

  // Initialize global globbed folders
  initGlobalConfigFolders(config, assets);

  // Validate session secret
  validateSessionSecret(config);

  // Expose configuration utilities
  config.utils = {
    getGlobbedPaths: getGlobbedPaths,
    validateSessionSecret: validateSessionSecret
  };

  return config;
};

/**
 * Set configuration object
 */
module.exports = initGlobalConfig();
