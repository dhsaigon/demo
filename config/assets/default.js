'use strict';

module.exports = {
  client: {
    lib: {
      cssEmbed: [
        '../../bootstrap/dist/css/bootstrap.min.css'
      ],
      jsEmbed: [
        '../../jquery/dist/jquery.min.js',
        '../../bootstrap/dist/js/bootstrap.min.js',
        '../../string/dist/string.min.js',
        '../../angular/angular.min.js',
        '../../angular-resource/angular-resource.min.js',
        '../../angular-animate/angular-animate.min.js',
        '../../angular-messages/angular-messages.min.js',
        '../../angular-ui-router/release/angular-ui-router.min.js',
        '../../angular-ui-utils/ui-utils.min.js',
        '../../angular-bootstrap/ui-bootstrap-tpls.min.js',
        '../../ngstorage/ngStorage.min.js',
        '../../tinymce/tinymce.min.js',
        '../../angular-ui-tinymce/dist/tinymce.min.js',
        '../../async/dist/async.min.js'
      ]
    },
    cssEmbed: [
      'embed-modules/*/client/css/*.css',
      'public/*/css/*.css'
    ],
    lessEmbed: [
      'embed-modules/*/client/less/*.less'
    ],
    sassEmbed: [
      'embed-modules/*/client/scss/*.scss'
    ],
    jsEmbed: [
      'embed-modules/core/client/app/config.js',
      'embed-modules/core/client/app/init.js',
      'embed-modules/*/client/*.js',
      'public/*/js/*.js',
      'embed-modules/*/client/**/*.js'
    ],
    imgEmbed: [
      'embed-modules/**/*/img/**/*.jpg',
      'embed-modules/**/*/img/**/*.png',
      'embed-modules/**/*/img/**/*.gif',
      'embed-modules/**/*/img/**/*.svg'
    ],
    viewsEmbed: ['embed-modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: ['gruntfile.js'],
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js', '*-modules/*/server/**/*.js'],
    models: ['modules/*/server/models/**/*.js', '*-modules/*/server/models/**/*.js'],
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js', '*-modules/!(core)/server/routes/**/*.js', '*-modules/core/server/routes/**/*.js'],
    config: ['modules/*/server/config/*.js', '*-modules/*/server/config/*.js'],
    views: ['modules/*/server/views/*.html', '*-modules/*/server/views/*.html']
  }
};
