var url = "https://storm-downtime-haravan.herokuapp.com/";
$('head').append('<script src="'+url+'top-ad/js/countdown.js"></script>');
$('head').append('<script src="'+url+'top-ad/js/flipclock.js"></script>');
$('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/cssdowntime.css"  />');
$.ajax({
    url: url+"get-data",
    type: "get", //send it through get method
    data: JSON.stringify({
        "shop_id": "123456"
    }), contentType: "application/json",

    success: function (response) {
        var background_color = response.background_color;
        var title_text_string = response.title_text_string;
        var title_text_color = response.title_text_color;
        var button_text_string = response.button_text_string;
        var button_text_color = response.button_text_color;
        var button_background = response.button_background;
        var button_url = response.button_url;
        var theme = response.theme;
        $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/flipclock-'+theme+'.css"  />');
        $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/top-ad.css"  />');

        var text =
            '<div  id="dw-countdown-widget-container" style="display: block;background: '+background_color+';color:'+title_text_color+'"> ' +
            '  <div  id="countdown-text">'
                        + title_text_string +
            '              </div>'+
            '<div class="tick" id="ticker" data-did-init="handleTickInit" data-state="initialised"></div>' +
            '        <a class="countdown-text countdown-button" style="color: '+button_text_color+';background: '+button_background+'"  id="dw-countdown-widget-container-button" target="_blank" href="'+button_url+'" > '+button_text_string+' </a>    ' +
            '</div>';
        $('body').prepend(text);
        setTimeout(function() {


            clock = $('#ticker').FlipClock({
                clockFace: 'DailyCounter',
                autoStart: false,
                callbacks: {
                    stop: function() {
                        $('.message').html('The clock has stopped!')
                    }
                }
            });

            clock.setTime(220860);
            clock.setCountdown(true);
            clock.start();
            $('.flip-clock-divider .flip-clock-label').css("cssText", "color: "+title_text_color);
            // $('.flip-clock-wrapper ul li a div div.inn').css("cssText", "text-shadow: 0 1px 1px #FFFF00;");
        }, 2000);
    }
});


