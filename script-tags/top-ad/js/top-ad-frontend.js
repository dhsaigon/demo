var url = "https://storm-downtime-haravan.herokuapp.com/";
var shop = window.location.host;
$('head').append('<script src="'+url+'top-ad/js/countdown.js"></script>');
$('head').append('<script src="'+url+'top-ad/js/flipclock.js"></script>');
$('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/cssdowntime.css"  />');
console.log('load top-ad.js');
$.ajax({
    url: url+"baseapp/embed/api/getItem",
    type: "get", //send it through get method
    data: {
        "shop": shop
    },
    contentType: "application/json",
    success: function (response) {//success
        console.log('-'+response.store_id+'-');
        if (response.store_id != ''){
            var background_color = response.backgroupColor;
            var title_text_string = response.text;
            var title_text_color = response.textcolor;
            var button_text_string = response.buttontext;
            var button_text_color = response.buttoncolor;
            var button_background = response.buttonbackgroup;
            var button_url = response.button_url;
            var theme = response.clock_theme;
            var show_widget_on_homepage_only = response.show_widget_on_homepage_only;
            var enable_widget = response.enable_widget;
            if(!enable_widget){
                return;
            }
            if(show_widget_on_homepage_only){
                if ( window.location.pathname != '/' ){
                    // Index (home) page
                    return;
                }
            }
            $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/flipclock-'+theme+'.css"  />');
            $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/top-ad-frontend.css"  />');
            var text =
                '<div  id="dw-countdown-widget-container" style="display: block;background: '+background_color+';color:'+title_text_color+'"> ' +
                '  <div  id="countdown-text">'
                            + title_text_string +
                '              </div>'+
                '<div class="tick" id="ticker-wrapper"  data-did-init="handleTickInit" data-state="initialised"><div id="ticker"></div></div>' +
                '        <a class="countdown-text countdown-button" style="color: '+button_text_color+';background: '+button_background+'"  id="dw-countdown-widget-container-button" target="_blank" href="'+button_url+'" > '+button_text_string+' </a>    ' +
                '</div>' +
                '<script>'+
                'window.onscroll = function() {myFunction()};'+
                'var header = document.getElementById("dw-countdown-widget-container");'+
                'var sticky = header.offsetTop;'+
                'function myFunction() {'+
                'if (window.pageYOffset > 0) {'+
                'document.getElementById("dw-countdown-widget-container").style.position = "fixed" '+
                '} else {'+
                'document.getElementById("dw-countdown-widget-container").style.position = "inherit" '+
                '}'+
                '}'+
                'setTimeout(function() { '+
                'document.getElementById("dw-countdown-widget-container").style.position = "inherit" '+
                '}, 7000);'+
                '</script>';
            var container = '<div id="temp-container" style="display: none"></div>';
            $('body').prepend($(container));
            $('#temp-container').prepend($(text));
            setTimeout(function() {


                clock = $('#ticker').FlipClock({
                    clockFace: 'DailyCounter',


                    autoStart: false,
                    callbacks: {
                        onCreate: function() {
                            // Do something
                            $('#dw-countdown-widget-container').css("display", "inherit");
                        }
                    }
                });
                var d = new Date().getTime()/1000;
                var number = response.datemillisecond - d;
                if(number < 0){
                    number = 0;
                }
                clock.setTime(number);
                clock.setCountdown(true);

                clock.start();

                $('.flip-clock-divider .flip-clock-label').css("cssText", "color: "+title_text_color);
                setTimeout(function() {
                    $("#temp-container").css("display","inherit");
                    setTimeout(function() {
                        $("#dw-countdown-widget-container").animate({right: '0'}, 'fast');
                    },1500);
                    
                },2500);

                // $('.flip-clock-wrapper').css("cssText", "width: 360px");
                // $('.flip-clock-wrapper ul li a div div.inn').css("cssText", "text-shadow: 0 1px 1px #FFFF00;");
            }, 2000);
        }

       // $("#dw-countdown-widget-container").css("position", "inherit");
    }//end success
});


