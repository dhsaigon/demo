var url = "https://storm-downtime-haravan.herokuapp.com/";

function process_widget() {
    var checked = $("#widget_check")[0].checked;
    if(checked){
        $("#dw-countdown-widget-container").css("display","initial");
    }else{
        $("#dw-countdown-widget-container").css("display","none");
    }
}

function process_theme() {
    if($("#dark-theme-check")[0].checked){
        dark_theme_clock();
    }else {
        light_theme_clock();
    }
}

$( ".sp-val" ).click(function() {
    var titleColor = $("#title-color").css("background-color");
    $('.flip-clock-divider .flip-clock-label').css("cssText", "color: "+titleColor);
});

function light_theme_clock() {
    $('link[rel=stylesheet][href~="/top-ad/css/flipclock-dark.css"]').remove();
    $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/flipclock-light.css"  />');
    var titleColor = $("#title-color").css("background-color");
    $('.flip-clock-divider .flip-clock-label').css("cssText", "color: "+titleColor);
}
function dark_theme_clock() {
    $('link[rel=stylesheet][href~="/top-ad/css/flipclock-light.css"]').remove();
    $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/flipclock-dark.css"  />');
    var titleColor = $("#title-color").css("background-color");
    $('.flip-clock-divider .flip-clock-label').css("cssText", "color: "+titleColor);
}

function sync_first_time() {
    process_theme();
    process_widget();
    update_button_url();
}

function update_button_url() {
    $(".countdown-button").attr("href", $("#button-url").val())
}

function goUrl(e) {
    var url = $(e).attr("href");
    var win = window.open(url, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Browser has blocked it
        alert('Please allow popups for this website');
    }
    // window.location.href = $(e).attr("href");

}

function init_clock() {
    var clock = $('#ticker').FlipClock({
        clockFace: 'DailyCounter',


        autoStart: false
    });
    var datesetupstr = $("#loopingtime").val();
    var datenow = new Date().getTime()/1000;
    var datesetup = new Date(datesetupstr).getTime()/1000;
    var number = datesetup - datenow;
    if(number < 0){
        number = 0;
    }
    clock.setTime(number);
    clock.setCountdown(true);
    clock.start();
}

setTimeout(function() {
    $('#select-hour').editableSelect({ effects: 'default' });
    $('#select-second').editableSelect({ effects: 'default' });
    $('#select-minute').editableSelect({ effects: 'default' });
    $("#loopingtime" ).change(function() {
        init_clock();
    });
    // var index = 0;
    // $("#select-hour > option").each(function() {
    //     $(this).text(index);
    //     $(this).val(index);
    //     index++;
    // });

    init_clock();
    sync_first_time();
    apply_safari(url);

}, 2000);

function apply_safari(url) {
    var userAgent = navigator.userAgent.toLowerCase();
    console.log("userAgent "+ userAgent);
    if (userAgent .indexOf('safari')!=-1){
        if(userAgent .indexOf('chrome')  > -1){
        }else if((userAgent .indexOf('opera')  > -1)||(userAgent .indexOf('opr')  > -1)){
        }else{
            //browser is safari, add css
            $('head').append('<link rel="stylesheet" type="text/css" href="'+url+'top-ad/css/safari.css"  />');
        }
    }
}