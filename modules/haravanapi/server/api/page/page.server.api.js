'use strict';

var PageAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

PageAPI.prototype.get = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/pages/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.page) {
      callback(null, data.page);
    } else {
      callback();
    }
  });
};

module.exports = PageAPI;
