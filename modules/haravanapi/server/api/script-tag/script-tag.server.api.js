'use strict';

var path = require('path');
var nlogger = require(path.resolve('./config/lib/nlogger'));

var ScriptTagApi = function (HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

ScriptTagApi.prototype.create = function (src, callback) {
  var self = this;
  try {
    var apipath = "/admin/script_tags.json";
    var postData = {
      "script_tag": {
        "event": "onload",
        "src": src
      }
    };
    self.HaravanAPI.post(apipath, postData, function (err, data, headers) {
      if (err) {
        callback(err);
      } else if (data && data.script_tag) {
        callback(null, data.script_tag);
      } else {
        callback();
      }
    });
  } catch (e) {
    nlogger.writelog(nlogger.NLOGGER_ERROR, e, { filename: __filename, fn: 'create' });
    callback();
  }
};

ScriptTagApi.prototype.delete = function (id, callback) {
  var self = this;
  if (id && id > 0) {
    var apipath = "/admin/script_tags/" + id + ".json";
    self.HaravanAPI.delete(apipath, function (err) {
      if (err) {
        callback(err);
      } else {
        callback();
      }
    });
  } else {
    callback();
  }
};

ScriptTagApi.prototype.list = function (callback) {
  var self = this;
  try {
    var apipath = "/admin/script_tags.json";
    self.HaravanAPI.get(apipath, function (err, data, headers) {
      if (err) {
        callback(err);
      } else if (data && data.script_tags) {
        callback(null, data.script_tags);
      } else {
        callback();
      }
    });
  } catch (e) {
    nlogger.writelog(nlogger.NLOGGER_ERROR, e, { filename: __filename, fn: 'list' });
    callback();
  }
};

module.exports = ScriptTagApi;
