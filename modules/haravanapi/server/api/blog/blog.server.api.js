'use strict';

var BlogAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

BlogAPI.prototype.get = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/blogs/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.blog) {
      callback(null, data.blog);
    } else {
      callback();
    }
  });
};

module.exports = BlogAPI;
