'use strict';

var MetafieldAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

MetafieldAPI.prototype.get = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/metafields/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.metafield) {
      callback(null, data.metafield)
    } else {
      callback();
    }
  });
};

MetafieldAPI.prototype.findByOwner = function(ownerId, page, limit, callback) {
  var _this = this;

  var apiPath = '/admin/metafields.json?owner_id=' + ownerId;

  if (page) {
    apiPath += '&page=' + page;
  }

  if (limit) {
    apiPath += '&limit=' + limit;
  }

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.metafields) {
      callback(null, data.metafields)
    } else {
      callback();
    }
  });
};

MetafieldAPI.prototype.findByOwnerAndKey = function(ownerId, namespace, key, callback) {
  var _this = this;

  var apiPath = '/admin/metafields.json?namespace=' + namespace + '&key=' + key + '&owner_id=' + ownerId;

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.metafields) {
      callback(null, data.metafields)
    } else {
      callback();
    }
  });
};

MetafieldAPI.prototype.findByKey = function(resource, namespace, key, callback) {
  var _this = this;

  var apiPath = '/admin/metafields.json?owner_resource=' + resource + '&namespace=' + namespace + '&key=' + key;

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.metafields) {
      callback(null, data.metafields)
    } else {
      callback();
    }
  });
};

MetafieldAPI.prototype.create = function(type, ownerId, metafieldData, callback) {
  var _this = this;
  var apiPath = '/admin/' + type + '/' + ownerId + '/metafields.json';
  var postData = { metafield : metafieldData };

  //console.log("apiPath: ", apiPath);
  //console.log("postData: ", JSON.stringify(postData));

  _this.HaravanAPI.post(apiPath, postData , function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.metafield) {
      callback(null, data.metafield);
    } else {
      callback();
    }
  });
};

MetafieldAPI.prototype.update = function(id, metafieldData, callback) {
  var _this = this;

  var apiPath = '/admin/metafields/' + id + '.json';

  _this.HaravanAPI.put(apiPath, { metafield: metafieldData }, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.metafield) {
      callback(null, data.metafield);
    } else {
      callback();
    }
  });
};

MetafieldAPI.prototype.delete = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/metafields/' + id + '.json';

  _this.HaravanAPI.delete(apiPath, function(err) {
    if(err) {
      return callback(err);
    }

    callback();
  });
};

module.exports = MetafieldAPI;
