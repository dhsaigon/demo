'use strict';

var SmartCollectionAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

SmartCollectionAPI.prototype.get = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/smart_collections/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.smart_collection) {
      callback(null, data.smart_collection);
    } else {
      callback();
    }
  });
};

module.exports = SmartCollectionAPI;
