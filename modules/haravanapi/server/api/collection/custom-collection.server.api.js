'use strict';

var CustomCollectionAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

CustomCollectionAPI.prototype.get = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/custom_collections/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.custom_collection) {
      callback(null, data.custom_collection);
    } else {
      callback();
    }
  });
};

module.exports = CustomCollectionAPI;
