'use strict';

var ProductAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

ProductAPI.prototype.get = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/products/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.product) {
      callback(null, data.product)
    } else {
      callback();
    }
  });
};

ProductAPI.prototype.create = function(productData, callback) {
  var _this = this;

  var apiPath = '/admin/products.json';

  _this.HaravanAPI.post(apiPath, { product: productData }, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.product) {
      callback(null, data.product);
    } else {
      callback();
    }
  });
};

ProductAPI.prototype.update = function(id, updateData, callback) {
  var _this = this;

  var apiPath = '/admin/products/' + id + '.json';

  _this.HaravanAPI.put(apiPath, { product: updateData }, function(err, data) {
    if(err) {
      callback(err);
    } else if(data && data.product) {
      callback(err, data.product);
    } else {
      callback();
    }
  });
};

ProductAPI.prototype.delete = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/products/' + id + '.json';

  _this.HaravanAPI.delete(apiPath, function(err) {
    if(err) {
      return callback(err);
    }

    callback();
  });
};

module.exports = ProductAPI;
