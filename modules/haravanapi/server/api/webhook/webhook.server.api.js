'use strict';

var querystring = require('querystring');

var WebHookAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

WebHookAPI.prototype.getById = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/webhooks/' + id + '.json';

  _this.HaravanAPI.get(apiPath, function(err, data, statusCode) {
    if(err) {
      callback(err);
    } else if(data && data.webhook) {
      callback(null, data.webhook)
    } else {
      callback();
    }
  });
};

WebHookAPI.prototype.create = function(dataJson, callback) {
  var _this = this;

  var apiPath = '/admin/webhooks.json';

  _this.HaravanAPI.post(apiPath, { webhook: dataJson }, function(err, data, statusCode) {
    if(err) {
      callback(err);
    } else if(data && data.webhook) {
      callback(null, data.webhook);
    } else {
      callback();
    }
  });
};

WebHookAPI.prototype.update = function(id, updateData, callback) {
  var _this = this;

  var apiPath = '/admin/webhooks/' + id + '.json';

  _this.HaravanAPI.put(apiPath, { webhook: updateData }, function(err, data, statusCode) {
    if(err) {
      callback(err);
    } else if(data && data.webhook) {
      callback(err, data.webhook);
    } else {
      callback();
    }
  });
};

WebHookAPI.prototype.delete = function(id, callback) {
  var _this = this;

  var apiPath = '/admin/webhooks/' + id + '.json';

  _this.HaravanAPI.delete(apiPath, function(err, data, statusCode) {
    if(err) {
      return callback(err);
    }

    callback();
  });
};

WebHookAPI.prototype.getCount = function(query, callback) {
  var _this = this;
  var apiPath = '/admin/webhooks/count.json';

  if(query && querystring.stringify(query)){
    apiPath += '?' + querystring.stringify(query);
  }

  _this.HaravanAPI.get(apiPath, function(err, data, statusCode) {
    if(err) {
      callback(err);
    } else if(data && data.count) {
      callback(null, data.count)
    } else {
      callback();
    }
  });
};

WebHookAPI.prototype.getList = function(query, callback) {
  var _this = this;
  var apiPath = '/admin/webhooks.json';

  if(query && querystring.stringify(query)){
    apiPath += '?' + querystring.stringify(query);
  }

  _this.HaravanAPI.get(apiPath, function(err, data, statusCode) {
    if(err) {
      callback(err);
    } else if(data && data.webhooks) {
      callback(null, data.webhooks)
    } else {
      callback();
    }
  });
};

module.exports = WebHookAPI;
