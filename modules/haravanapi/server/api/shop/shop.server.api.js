'use strict';

var path = require('path');
var nlogger = require(path.resolve('./config/lib/nlogger'));

var ShopAPI = function(HaravanAPI) {
  this.HaravanAPI = HaravanAPI;
};

ShopAPI.prototype.get = function(callback) {
  var _this = this;
  var apiPath = '/admin/shop.json';

  _this.HaravanAPI.get(apiPath, function(err, data, statusCode) {
    if(err) {
      nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
        shop: _this.HaravanAPI.config.shop,
        apiPath: apiPath,
        statusCode: statusCode || '',
        filename: __filename,
        fn: 'ShopAPI#get',
        data: data
      });
      callback(err);
    } else if(data && data.shop) {
      callback(null, data.shop)
    } else {
      callback();
    }
  });
};

module.exports = ShopAPI;
